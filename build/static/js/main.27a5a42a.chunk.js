(window.webpackJsonp = window.webpackJsonp || []).push([
  [0],
  {
    17: function(t, n, e) {
      t.exports = e(28);
    },
    28: function(t, n, e) {
      "use strict";
      e.r(n);
      var i = e(0),
        a = e.n(i),
        r = e(12),
        l = e.n(r),
        c = e(3),
        o = e(4),
        s = e(6),
        d = e(5),
        m = e(7),
        p = e(1),
        h = e(2);
      function u() {
        var t = Object(p.a)([
          "\n  max-width: 50vw;\n  max-height: 50vh;\n  display: block;\n  margin: auto;\n  border: 1px;\n  border-color: #222;\n  border-style: double;\n  ::selection {\n  background: white; /* WebKit/Blink Browsers */\n}\n  @media only screen and (max-width: 420px) {\n    max-width: 85vw;\n    \n  }\n"
        ]);
        return (
          (u = function() {
            return t;
          }),
          t
        );
      }
      var f = h.a.img(u()),
        g = function(t) {
          var n = t.activeThumbnail;
          return a.a.createElement(f, {
            src: n.imgUrl,
            alt: "colorless photograph"
          });
        };
      function b() {
        var t = Object(p.a)([
          "\n  max-width: 12.5vw;\n  max-height: 3.5vh;\n  justify-content: center;\n  margin-left: 3px;\n  margin-right: 3px;\n  filter: invert(100%);\n  display: flex;\n\n  ::selection {\n  background: white; /* WebKit/Blink Browsers */\n  }\n\n  &:hover {\n    filter: invert(0%);\n  }\n  &:active {\n    filter: invert(0%);\n  }\n\n  @media only screen and (max-width: 420px) {\n    max-width: 17.5vw;\n    max-height: 19.5vh;\n    margin: 2px;\n  }\n}\n"
        ]);
        return (
          (b = function() {
            return t;
          }),
          t
        );
      }
      var _ = h.a.img(b()),
        k = function(t) {
          var n = t.imgUrl,
            e = t.handleClick,
            i = t.index;
          return a.a.createElement(_, {
            src: n,
            onClick: e,
            "data-index": i,
            alt: "colorless photograp"
          });
        };
      function v() {
        var t = Object(p.a)([
          "\n  min-height: 100px;\n  margin-top: 20px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  overflow-x: auto;\n  border: 1px black;\n  ::selection {\n  background: white; /* WebKit/Blink Browsers */\n}\n\n  &::-webkit-scrollbar {\n    display: none;\n  }\n\n  @media only screen and (max-width: 420px) {\n    margin-top: 5vh;\n  }\n"
        ]);
        return (
          (v = function() {
            return t;
          }),
          t
        );
      }
      var w = h.a.div(v()),
        j = function(t) {
          var n = t.thumbnails,
            e = t.handleClick;
          return a.a.createElement(
            w,
            null,
            n.map(function(t, n) {
              return a.a.createElement(k, {
                key: t.imgUrl,
                imgUrl: t.imgUrl,
                handleClick: e,
                index: n
              });
            })
          );
        };
      function x() {
        var t = Object(p.a)([
          "\n  color: #222;\n  font-size: 90%;\n  margin-left: 15px;\n  margin-bottom: 10px;\n  text-align: center;\n  font-weight: normal;\n  ::selection {\n  background: white; /* WebKit/Blink Browsers */\n}\n  @media only screen and (min-width: 420px) {\n    margin-left: 0px;\n  }\n"
        ]);
        return (
          (x = function() {
            return t;
          }),
          t
        );
      }
      var y = (function(t) {
          function n() {
            var t, e;
            Object(c.a)(this, n);
            for (var i = arguments.length, r = new Array(i), l = 0; l < i; l++)
              r[l] = arguments[l];
            return (
              ((e = Object(s.a)(
                this,
                (t = Object(d.a)(n)).call.apply(t, [this].concat(r))
              )).state = {
                thumbnails: [
                  {
                    imgUrl:
                      "https://live.staticflickr.com/3691/13629594364_88bee8f06d_h.jpg",
                    title: "Bergen_2013_018.jpg",
                    link:
                      "https://www.flickr.com/photos/arienshibani/13629594364/in/album-72157654900464366/",
                    desc: "Hugo Jumps",
                    largeDownload:
                      "https://live.staticflickr.com/3691/13629594364_6c23a506e2_o_d.jpg",
                    smallDownload:
                      "https://live.staticflickr.com/3691/13629594364_0a6acb6bda_z_d.jpg"
                  },
                  {
                    imgUrl:
                      "https://live.staticflickr.com/7373/12622816843_5c529ee89b_h.jpg",
                    title: "Berlin_2014_002.jpg",
                    link:
                      "https://www.flickr.com/photos/arienshibani/13629594364/in/album-72157654900464366/",
                    desc: "Berlin Bridge",
                    largeDownload:
                      "https://live.staticflickr.com/7373/12622816843_ba2be911f3_o_d.jpg",
                    smallDownload:
                      "https://live.staticflickr.com/7373/12622816843_e5d7ab4b3c_z_d.jpg"
                  },
                  {
                    imgUrl:
                      "https://live.staticflickr.com/2812/13569365074_40ef2d02c1_h.jpg",
                    title: "Bergen_2013_062.jpg",
                    link:
                      "https://www.flickr.com/photos/arienshibani/13629594364/in/album-72157654900464366/",
                    desc: "Reflections In Water",
                    largeDownload:
                      "https://live.staticflickr.com/2812/13569365074_40ef2d02c1_h_d.jpg",
                    smallDownload:
                      "https://live.staticflickr.com/2812/13569365074_0ec049f2be_z_d.jpg"
                  },
                  {
                    imgUrl:
                      "https://live.staticflickr.com/2823/13245155404_2e90284785_h.jpg",
                    title: "Berlin_2014_028.jpg",
                    link:
                      "https://www.flickr.com/photos/arienshibani/13629594364/in/album-72157654900464366/",
                    desc: "Berlin Dog",
                    largeDownload:
                      "https://live.staticflickr.com/2823/13245155404_c53e518632_o_d.jpg",
                    smallDownload:
                      "https://live.staticflickr.com/2823/13245155404_8ee1445e18_z_d.jpg"
                  },
                  {
                    imgUrl:
                      "https://live.staticflickr.com/3818/13245156424_b1074b0d58_h.jpg",
                    title: "Dublin_2014_015.jpg",
                    link:
                      "https://www.flickr.com/photos/arienshibani/13629594364/in/album-72157654900464366/",
                    desc: "Statue",
                    largeDownload:
                      "https://live.staticflickr.com/3818/13245156424_c96ffdd231_o_d.jpg",
                    smallDownload:
                      "https://live.staticflickr.com/3818/13245156424_6bff9924f4_z_d.jpg"
                  },
                  {
                    imgUrl:
                      "https://live.staticflickr.com/3726/13244975883_e7b5908974_h.jpg",
                    title: "Dublin_2014_001.jpg",
                    link:
                      "https://www.flickr.com/photos/arienshibani/13629594364/in/album-72157654900464366/",
                    desc: "Stardust",
                    largeDownload:
                      "https://live.staticflickr.com/3726/13244975883_f0c2362e15_o_d.jpg",
                    smallDownload:
                      "https://live.staticflickr.com/3726/13244975883_57da671c08_z_d.jpg"
                  },
                  {
                    imgUrl:
                      "https://live.staticflickr.com/7276/13569009335_381e99e4af_h.jpg",
                    title: "Bergen_2013_009.jpg",
                    link:
                      "https://www.flickr.com/photos/arienshibani/13629594364/in/album-72157654900464366/",
                    desc: "Trees",
                    largeDownload:
                      "https://live.staticflickr.com/7276/13569009335_7a11c74a09_o_d.jpg",
                    smallDownload:
                      "https://live.staticflickr.com/7276/13569009335_c0ddfb7ecd_z_d.jpg"
                  },
                  {
                    imgUrl:
                      "https://live.staticflickr.com/2893/12622037633_efea191a40_h.jpg",
                    title: "Berlin_2014_102.jpg",
                    link:
                      "https://www.flickr.com/photos/arienshibani/13629594364/in/album-72157654900464366/",
                    desc: "TV",
                    largeDownload:
                      "https://live.staticflickr.com/2893/12622037633_c5c7d5df27_o_d.jpg",
                    smallDownload:
                      "https://live.staticflickr.com/2893/12622037633_366ea6d6c9_z_d.jpg"
                  },
                  {
                    imgUrl:
                      "https://live.staticflickr.com/65535/48803690463_b4240ffb2c_h.jpg",
                    title: "Berlin_2017_001.jpg",
                    link:
                      "https://www.flickr.com/photos/arienshibani/13629594364/in/album-72157654900464366/",
                    desc: "Trees",
                    largeDownload:
                      "https://live.staticflickr.com/7276/13569009335_7a11c74a09_o_d.jpg",
                    smallDownload:
                      "https://live.staticflickr.com/7276/13569009335_c0ddfb7ecd_z_d.jpg"
                  },
                  {
                    imgUrl:
                      "https://live.staticflickr.com/65535/48626710746_5f1dad83d4_h.jpg",
                    title: "Rijeka_2017_003.jpg",
                    link:
                      "https://live.staticflickr.com/65535/48626710746_5f1dad83d4_h.jpg",
                    desc: "Rijeka Beach",
                    largeDownload:
                      "https://live.staticflickr.com/2893/12622037633_c5c7d5df27_o_d.jpg",
                    smallDownload:
                      "https://live.staticflickr.com/2893/12622037633_366ea6d6c9_z_d.jpg"
                  }
                ],
                activeIndex: Math.floor(7 * Math.random()) + 1
              }),
              (e.handleClick = function(t) {
                var n = t.target.getAttribute("data-index");
                e.setState({ activeIndex: n });
              }),
              (e.renderTextContent = function() {
                var t = e.state,
                  n = t.thumbnails,
                  i = t.activeIndex;
                return a.a.createElement(
                  a.a.Fragment,
                  null,
                  a.a.createElement("h1", null, " ", n[i].title, " ")
                );
              }),
              e
            );
          }
          return (
            Object(m.a)(n, t),
            Object(o.a)(n, [
              {
                key: "render",
                value: function() {
                  var t = this.state,
                    n = t.thumbnails,
                    e = t.activeIndex,
                    i = n[e].title;
                  return (
                    (i = i.substring(0, i.length - 4)),
                    a.a.createElement(
                      "div",
                      null,
                      a.a.createElement(g, { activeThumbnail: n[e] }),
                      a.a.createElement(O, null, i),
                      a.a.createElement(j, {
                        thumbnails: n,
                        handleClick: this.handleClick
                      })
                    )
                  );
                }
              }
            ]),
            n
          );
        })(i.Component),
        O = h.a.h1(x());
      function B() {
        var t = Object(p.a)([
          "\nfont-size: 10px;\n::selection {\n  background: white; /* WebKit/Blink Browsers */\n}"
        ]);
        return (
          (B = function() {
            return t;
          }),
          t
        );
      }
      function D() {
        var t = Object(p.a)([
          "\n  align-items: center;\n  display: flex;\n  margin-bottom: -2vh;\n  margin-top: 2vh;\n  color: black;\n  justify-content: center;\n  text-decoration: none;\n  max-width: 100%;\n  font-weight: bold;\n  background-position: bottom;\n  background-repeat: repeat-x;\n  border-bottom: 2;\n  text-decoration: none;\n  ::selection {\n  background: white; /* WebKit/Blink Browsers */\n}"
        ]);
        return (
          (D = function() {
            return t;
          }),
          t
        );
      }
      function E() {
        var t = Object(p.a)([
          "\n  text-align: center;\n  background: #fff0;\n  font-size: 60px;\n  padding-left: 50px;\n  padding-right: 50px;\n"
        ]);
        return (
          (E = function() {
            return t;
          }),
          t
        );
      }
      var z = h.a.header(E()),
        C = h.a.p(D()),
        U = h.a.p(B()),
        I = (function(t) {
          function n() {
            return (
              Object(c.a)(this, n),
              Object(s.a)(this, Object(d.a)(n).apply(this, arguments))
            );
          }
          return (
            Object(m.a)(n, t),
            Object(o.a)(n, [
              {
                key: "render",
                value: function() {
                  return a.a.createElement(
                    z,
                    null,
                    a.a.createElement(C, null, "SHIBANI"),
                    a.a.createElement(U, null, "120mm B/W")
                  );
                }
              }
            ]),
            n
          );
        })(i.Component);
      function W() {
        var t = Object(p.a)([
          "\n  --mainColor: #222;\n\n   font-size: 100%;\n    background: linear-gradient(\n      to bottom, var(--mainColor) 0%,\n        var(--mainColor) 100%\n      );\n\n    background-position: 0 100%;\n    background-repeat: repeat-x;\n    background-size: 2px 2px;\n    color: #000;\n    text-decoration: none;\n    transition: background-size 0.5s;\n    margin-left: 10px;\n\n    :hover {\n    background-size: 4px 50px;\n    color: #fff}\n\n    display: none;\n    \n    @media only screen and (max-width: 420px) {\n      display: none;\n    }"
        ]);
        return (
          (W = function() {
            return t;
          }),
          t
        );
      }
      function K() {
        var t = Object(p.a)([
          "\n  position: fixed;\n  left: 0;\n  bottom: 0;\n  width: 100%;\n  height: 5%\n  color: #222;\n  text-align: center;\n"
        ]);
        return (
          (K = function() {
            return t;
          }),
          t
        );
      }
      var T = h.a.footer(K()),
        S = h.a.a(W()),
        A = (function(t) {
          function n() {
            return (
              Object(c.a)(this, n),
              Object(s.a)(this, Object(d.a)(n).apply(this, arguments))
            );
          }
          return (
            Object(m.a)(n, t),
            Object(o.a)(n, [
              {
                key: "render",
                value: function() {
                  return a.a.createElement(
                    T,
                    null,
                    a.a.createElement(
                      S,
                      { href: "mailto:aaari94@gmail.com" },
                      "Contact"
                    )
                  );
                }
              }
            ]),
            n
          );
        })(i.Component),
        H = (function(t) {
          function n() {
            return (
              Object(c.a)(this, n),
              Object(s.a)(this, Object(d.a)(n).apply(this, arguments))
            );
          }
          return (
            Object(m.a)(n, t),
            Object(o.a)(n, [
              {
                key: "render",
                value: function() {
                  return a.a.createElement(
                    "div",
                    { style: J },
                    a.a.createElement(I, null),
                    a.a.createElement(y, null),
                    a.a.createElement(A, null)
                  );
                }
              }
            ]),
            n
          );
        })(i.Component),
        J = { fontFamily: "Helvetica" };
      l.a.render(a.a.createElement(H, null), document.getElementById("root"));
    }
  },
  [[17, 1, 2]]
]);
//# sourceMappingURL=main.27a5a42a.chunk.js.map
