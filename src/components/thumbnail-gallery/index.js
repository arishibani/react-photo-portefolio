import React, { Component } from "react";
import ActiveThumbnailWindow from "./active-thumbnail-window";
import ThumbnailGrid from "./thumbnail-grid/thumbnail-grid";
import styled from "styled-components";
//import axios from "axios"; FLICKR API FUNKE KJE LENGER :(((((

export default class ThumbnailGallery extends Component {
  state = {
    thumbnails: [
      {
        imgUrl:
          "https://live.staticflickr.com/3767/12621267975_3b17e2e0bb_h.jpg",
        title: "Berlin_2014_003.jpg",
        link: "",
        desc: "Stazi Museum",
        largeDownload: "",
        smallDownload: ""
      },
      {
        imgUrl:
          "https://live.staticflickr.com/3691/13629594364_88bee8f06d_h.jpg",
        title: "Bergen_2013_018.jpg",
        link:
          "https://www.flickr.com/photos/arienshibani/13629594364/in/album-72157654900464366/",
        desc: "Hugo Jumps",
        largeDownload:
          "https://live.staticflickr.com/3691/13629594364_6c23a506e2_o_d.jpg",
        smallDownload:
          "https://live.staticflickr.com/3691/13629594364_0a6acb6bda_z_d.jpg"
      },
      {
        imgUrl:
          "https://live.staticflickr.com/7373/12622816843_5c529ee89b_h.jpg",
        title: "Berlin_2014_002.jpg",
        link:
          "https://www.flickr.com/photos/arienshibani/13629594364/in/album-72157654900464366/",
        desc: "Berlin Bridge",
        largeDownload:
          "https://live.staticflickr.com/7373/12622816843_ba2be911f3_o_d.jpg",
        smallDownload:
          "https://live.staticflickr.com/7373/12622816843_e5d7ab4b3c_z_d.jpg"
      },
      {
        imgUrl:
          "https://live.staticflickr.com/2812/13569365074_40ef2d02c1_h.jpg",
        title: "Bergen_2013_062.jpg",
        link:
          "https://www.flickr.com/photos/arienshibani/13629594364/in/album-72157654900464366/",
        desc: "Reflections In Water",
        largeDownload:
          "https://live.staticflickr.com/2812/13569365074_40ef2d02c1_h_d.jpg",
        smallDownload:
          "https://live.staticflickr.com/2812/13569365074_0ec049f2be_z_d.jpg"
      },
      {
        imgUrl:
          "https://live.staticflickr.com/2823/13245155404_2e90284785_h.jpg",
        title: "Berlin_2014_028.jpg",
        link:
          "https://www.flickr.com/photos/arienshibani/13629594364/in/album-72157654900464366/",
        desc: "Berlin Dog",
        largeDownload:
          "https://live.staticflickr.com/2823/13245155404_c53e518632_o_d.jpg",
        smallDownload:
          "https://live.staticflickr.com/2823/13245155404_8ee1445e18_z_d.jpg"
      },
      {
        imgUrl:
          "https://live.staticflickr.com/3818/13245156424_b1074b0d58_h.jpg",
        title: "Dublin_2014_015.jpg",
        link:
          "https://www.flickr.com/photos/arienshibani/13629594364/in/album-72157654900464366/",
        desc: "Statue",
        largeDownload:
          "https://live.staticflickr.com/3818/13245156424_c96ffdd231_o_d.jpg",
        smallDownload:
          "https://live.staticflickr.com/3818/13245156424_6bff9924f4_z_d.jpg"
      },
      {
        imgUrl:
          "https://live.staticflickr.com/3726/13244975883_e7b5908974_h.jpg",
        title: "Dublin_2014_001.jpg",
        link:
          "https://www.flickr.com/photos/arienshibani/13629594364/in/album-72157654900464366/",
        desc: "Stardust",
        largeDownload:
          "https://live.staticflickr.com/3726/13244975883_f0c2362e15_o_d.jpg",
        smallDownload:
          "https://live.staticflickr.com/3726/13244975883_57da671c08_z_d.jpg"
      },
      {
        imgUrl:
          "https://live.staticflickr.com/7276/13569009335_381e99e4af_h.jpg",
        title: "Bergen_2013_009.jpg",
        link:
          "https://www.flickr.com/photos/arienshibani/13629594364/in/album-72157654900464366/",
        desc: "Trees",
        largeDownload:
          "https://live.staticflickr.com/7276/13569009335_7a11c74a09_o_d.jpg",
        smallDownload:
          "https://live.staticflickr.com/7276/13569009335_c0ddfb7ecd_z_d.jpg"
      },
      {
        imgUrl:
          "https://live.staticflickr.com/2893/12622037633_efea191a40_h.jpg",
        title: "Berlin_2014_102.jpg",
        link:
          "https://www.flickr.com/photos/arienshibani/13629594364/in/album-72157654900464366/",
        desc: "TV",
        largeDownload:
          "https://live.staticflickr.com/2893/12622037633_c5c7d5df27_o_d.jpg",
        smallDownload:
          "https://live.staticflickr.com/2893/12622037633_366ea6d6c9_z_d.jpg"
      },
      {
        imgUrl:
          "https://live.staticflickr.com/65535/48803690463_b4240ffb2c_h.jpg",
        title: "Berlin_2017_001.jpg",
        link:
          "https://www.flickr.com/photos/arienshibani/13629594364/in/album-72157654900464366/",
        desc: "Trees",
        largeDownload:
          "https://live.staticflickr.com/7276/13569009335_7a11c74a09_o_d.jpg",
        smallDownload:
          "https://live.staticflickr.com/7276/13569009335_c0ddfb7ecd_z_d.jpg"
      },
      {
        imgUrl:
          "https://live.staticflickr.com/65535/48626710746_5f1dad83d4_h.jpg",
        title: "Rijeka_2017_003.jpg",
        link:
          "https://live.staticflickr.com/65535/48626710746_5f1dad83d4_h.jpg",
        desc: "Rijeka Beach",
        largeDownload:
          "https://live.staticflickr.com/2893/12622037633_c5c7d5df27_o_d.jpg",
        smallDownload:
          "https://live.staticflickr.com/2893/12622037633_366ea6d6c9_z_d.jpg"
      }
    ],
    activeIndex: Math.floor(Math.random() * 10) + 1
  };

  handleClick = e => {
    const newActiveIndex = e.target.getAttribute("data-index");
    this.setState({ activeIndex: newActiveIndex });
  };

  render() {
    const { thumbnails, activeIndex } = this.state;
    var str = thumbnails[activeIndex].title;
    str = str.substring(0, str.length - 4);

    return (
      <StyledGallery>
        <ActiveThumbnailWindow activeThumbnail={thumbnails[activeIndex]} />
        <ImageTitle> {str}</ImageTitle>
        <ImagePrice>19.000,- NOK</ImagePrice>
        <ThumbnailGrid thumbnails={thumbnails} handleClick={this.handleClick} />
      </StyledGallery>
    );
  }
}

const StyledGallery = styled.div``;

const ImageTitle = styled.h1`
  color: white;
  font-size: 90%;
  margin-left: 20px;
  margin-bottom: 20px;
  margin-top: 20px;

  font-weight: normal;
  ::selection {
    background: white; /* WebKit/Blink Browsers */
  }
  @media only screen and (min-width: 420px) {
    margin-left: 0px;
    text-align: center;
  }
`;

const ImagePrice = styled.h1`
  color: white;
  font-size: 90%;
  margin-left: 20px;
  margin-bottom: 20px;
  margin-top: 20px;

  font-weight: normal;
  ::selection {
    background: white; /* WebKit/Blink Browsers */
  }
  @media only screen and (min-width: 420px) {
    margin-left: 0px;
    text-align: center;
    display: none;
  }
`;
