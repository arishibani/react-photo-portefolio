import React, { Component } from "react";
import ThumbnailGallery from "./thumbnail-gallery";
import Header from "../components/header/header";

export default class App extends Component {
  render() {
    return (
      <div style={topLevelStyles}>
        <Header />
        <ThumbnailGallery />
      </div>
    );
  }
}

const topLevelStyles = {
  fontFamily: "Helvetica",
  maxHeight: "100vh"
};
