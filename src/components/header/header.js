import React, { Component } from "react";
import styled from "styled-components";

const StyledHeader = styled.header`
  text-align: center;
  font-size: min(max(2rem, 8vw), 44px);
  padding: 4vh;

  @media only screen and (max-width: 420px) {
    padding: 3vh;
  }
`;

const SquigglyHeader = styled.p`
  align-items: center;
  display: flex;
  margin-bottom: -1vh;
  margin-top: 2vh;
  color: white;
  justify-content: center;
  text-decoration: none;

  font-weight: bold;
  background-position: bottom;
  background-repeat: repeat-x;
  border-bottom: 2;
  text-decoration: none;
  ::selection {
    background: white; /* WebKit/Blink Browsers */
  }
`;

const SubHeader = styled.p`
  font-size: 12px;
  color: white;
  ::selection {
    background: white; /* WebKit/Blink Browsers */
  }

  @media only screen and (max-width: 420px) {
  }
`;

export default class Header extends Component {
  render() {
    return (
      <StyledHeader>
        <SquigglyHeader>SHIBANI</SquigglyHeader>
        <SubHeader>120mm B/W</SubHeader>
      </StyledHeader>
    );
  }
}
